<?php

namespace AppBundle\Command;

use AppBundle\Entity\User;
use \Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Vladislav Iavorskii
 */
class UserListCommand extends ContainerAwareCommand
{

    private static $fileName = 'addrs.csv';

    private static $user = 'admin';
    private static $password = 'turtle';


    protected function configure()
    {
        $this->setName('app:import:user:list');
        $this->setDescription("Import user list CSV file");
        $this->addArgument('path', InputArgument::REQUIRED, 'Path to CSV file');
        $this->addOption('refId', null, InputOption::VALUE_REQUIRED, "Number of user list");
    }
    
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $refId = $input->getOption('refId');
        $path = $input->getArgument('path');

        $this->downloadFile($refId, $path);

        $filePath = $path . '/' .self::$fileName;

        $em = $this->getContainer()->get("doctrine.orm.entity_manager");

        if (($handle = fopen($filePath, "r")) !== false) {
            $rows = [];
            while (($data = fgetcsv($handle, null, ",")) !== false) {
                $userId = $data[0];
                $opened = $data[1];
                $clicked = $data[2];

                $userRepo = $em->getRepository(User::class);
                $userExist = $userRepo->findOneBy(['globalId' => $userId, 'refId' => $refId]);
                if (empty($userExist)) {
                    $user = new User();
                    $user->setGlobalId($userId)
                        ->setRefId($refId)
                        ->setOpened($opened)
                        ->setClicked($clicked)
                    ;

                    $em->persist($user);
                } else {
                    $userExist
                        ->setOpened($opened)
                        ->setClicked($clicked)
                    ;
                    $em->persist($userExist);

                }
            }
            fclose($handle);

            $em->flush();
        }
    }


    private function downloadFile($refId, $path)
    {
        $source = "http://getter.poctomep.ru/inbox/index.php?lid={$refId}";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $source);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION,3);
        curl_setopt($ch, CURLOPT_USERPWD, self::$user . ":" . self::$password);
        $data = curl_exec ($ch);
        $error = curl_error($ch);
        curl_close ($ch);

        $destination = $path . '/' . self::$fileName;
        $file = fopen($destination, "w+");
        fputs($file, $data);
        fclose($file);
    }
}