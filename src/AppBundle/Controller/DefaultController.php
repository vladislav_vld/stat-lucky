<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Stat;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DefaultController extends Controller
{
    private static $secretKey = "1a8f104206e558e73ae67fb434e6ae38";

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $userIds = $request->get('user_id');
        $refId = $request->get('ref_id');
        $statService = $this->get('appbundle.stat.service');

        $stat = $statService->getStat($userIds, $refId);

        return $this->render('default/index.html.twig', [
            'stat' => $stat,
            'userIds' => $userIds,
            'refId' => $refId
        ]);
    }

    /**
     * @Route("/details", name="details")
     */
    public function detailsAction(Request $request)
    {
        $userIds = $request->get('user_id');
        $refId = $request->get('ref_id');
        $eventId = $request->get('event_id');



        $statService = $this->get('appbundle.stat.service');

        $stat = $statService->getDetailStat($userIds, $refId, $eventId);
        return $this->render('default/details.html.twig', [
            'stat' => $stat,
            'userIds' => $userIds,
            'refId' => $refId,
            'eventId' =>$eventId
        ]);
    }

    /**
     * @Route("/stat", name="stat")
     */
    public function statAction(Request $request)
    {
        $key = $request->get('key');
        if (!$key == self::$secretKey) {
            $e = $this->createAccessDeniedException();
            throw $e;
        }
        $params = $request->query->all();
        $file = fopen('params.txt', 'a');
        fwrite($file, json_encode($params) . "\r\n");
        fclose($file);
        $userId = $request->get('user_id');
        $type = $request->get('type');
        $amount = $request->get('amount');

        if (!in_array($type, [1,2,3])) {
            throw new BadRequestHttpException('type is incorrect');
        }

        if (empty($userId) || empty($type)) {
            throw new BadRequestHttpException('user_id and type are required');
        }

        $em = $this->get("doctrine.orm.entity_manager");

        //it's possible that we need to find user by ref_id as well to filter extra in the future
        $userRepo = $em->getRepository(User::class);
        /** @var User|null $user */
        $user = $userRepo->findOneBy(['globalId' => $userId]);
        if (!empty($user)) {
            $stat = new Stat();
            $stat->setUserId($userId);
            $stat->setEventId($type);
            $stat->setDateEvent(new \DateTime());
            if ($amount) {
                $stat->setAmount($amount);
            }
            $em->persist($stat);
            $em->flush($stat);
        } else {
            $e = $this->createNotFoundException('User not found!');
            throw $e;
        }

        $response = new Response(json_encode(array('status' => 'ok')));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
