<?php

namespace AppBundle\Services;
use AppBundle\AppBundle;
use AppBundle\Entity\Stat;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * @author Vladislav Iavorskii
 */
class StatService
{

    /**
     * @var EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getStat($userId, $refId)
    {

        $userRepo = $this->em->getRepository(User::class);
        $user = $userRepo->findOneBy(["globalId" => $userId, "refId" => $refId]);
        $result = [];
        if ((empty($userId) && !empty($refId)) || !empty($user)) {
            $userRepo = $this->em->getRepository(User::class);

            $queryBuilder = $userRepo->createQueryBuilder('u');
            $queryBuilder
                ->select('COUNT(st.userId) as eventCount, st.eventId, u.globalId, u.opened, u.clicked')
                ->leftJoin("AppBundle\Entity\Stat", 'st', Join::LEFT_JOIN, $this->em->createQueryBuilder()->expr()->eq('st.userId', 'u.globalId'))
                ->groupBy("st.eventId, u.globalId, u.opened, u.clicked")
                ->where('1=1')
                ->having("u.opened > 0 or u.clicked > 0 or eventCount > 0")
                ->orderBy('eventCount', 'DESC')
            ;

            if (!empty($user)) {
                $this->applyUserFilter($queryBuilder, $userId);
            } elseif ((empty($user) && !empty($refId))) {
                $this->applyRefFilter($queryBuilder, $refId);

            }
//            echo $queryBuilder->getQuery()->getSQL();exit;
            $resultQuery = $queryBuilder->getQuery()->getResult();
            $result = [];
            foreach ($resultQuery as $item) {
                $result[$item['globalId']][$item['eventId']] = $item['eventCount'];
                $result[$item['globalId']]['clicked'] = $item['clicked'];
                $result[$item['globalId']]['opened'] = $item['opened'];
            }
        }

        return $result;
    }


    private function applyUserFilter(QueryBuilder $queryBuilder, $userId)
    {
        $queryBuilder
            ->andWhere("u.globalId = :userId")
            ->setParameter('userId', $userId)
        ;
    }

    private function applyRefFilter(QueryBuilder $queryBuilder, $refId)
    {
        $queryBuilder
            ->andWhere("u.refId = :refId")
            ->setParameter('refId', $refId)
        ;
    }


    public function getDetailStat($userId, $refId, $eventId)
    {
        $userRepo = $this->em->getRepository(User::class);
        $user = $userRepo->findOneBy(["globalId" => $userId, "refId" => $refId]);
        $result = [];
        if (!empty($user)) {
            $statRepo = $this->em->getRepository(Stat::class);
            $result = $statRepo->findBy(['userId' => $userId, 'eventId' => $eventId]);
        }

        return $result;
    }
}